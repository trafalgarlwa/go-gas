package render

import (
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"path/filepath"
	"strings"
)

type Options struct {
	Directory  string
	Extensions []string
}

type Render struct {
	opt       Options
	templates *template.Template
}

func New(opt ...Options) *Render {
	r := &Render{}

	if len(opt) > 0 {
		r.opt = opt[0]
	}

	r.prepareOptions()
	r.compileTemplates()

	return r
}

func (r *Render) prepareOptions() {
	if len(r.opt.Directory) == 0 {
		r.opt.Directory = "templates"
	}
	if len(r.opt.Extensions) == 0 {
		r.opt.Extensions = []string{".tmpl"}
	}
}

func (r *Render) compileTemplates() {
	dir := r.opt.Directory
	r.templates = template.New(dir)

	filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if info == nil || info.IsDir() {
			return nil
		}

		rel, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}

		ext := ""
		if strings.Index(rel, ".") != -1 {
			ext = filepath.Ext(rel)
		}

		for _, extension := range r.opt.Extensions {
			if ext == extension {
				buf, err := ioutil.ReadFile(path)
				if err != nil {
					panic(err)
				}
				name := rel[0 : len(rel)-len(ext)]
				tmpl := r.templates.New(filepath.ToSlash(name))
				template.Must(tmpl.Parse(string(buf)))
				break
			}
		}
		return nil
	})
}

func (r *Render) Html(w io.Writer, name string, data interface{}) error {
	return r.templates.ExecuteTemplate(w, name, data)
}
