create database if not exists gas_db;
use gas_db;

CREATE TABLE device
(
    id   INTEGER PRIMARY KEY AUTO_INCREMENT,
    uuid CHAR(40)    NOT NULL default '',
    name varchar(32) NOT NULL default ''
);

CREATE UNIQUE INDEX device_uuid_index ON device (uuid);

CREATE TABLE device_record
(
    id          INTEGER PRIMARY KEY AUTO_INCREMENT,
    device_id   INTEGER NOT NULL,
    env         DOUBLE  NOT NULL DEFAULT 0,
    ch4         DOUBLE  NOT NULL,
    c2h4        DOUBLE  NOT NULL,
    c3h8        DOUBLE  NOT NULL,
    c4h8        DOUBLE  NOT NULL,
    lower_limit DOUBLE  NOT NULL,
    upper_limit DOUBLE  NOT NULL,
    exposed     INT     NOT NULL DEFAULT 0,
    created_at  BIGINT  NOT NULL
);

CREATE INDEX device_record_created_at_index ON device_record (created_at);

CREATE TABLE user
(
    id         INTEGER PRIMARY KEY AUTO_INCREMENT,
    username   VARCHAR(16) NOT NULL UNIQUE,
    password   VARCHAR(32) NOT NULL,
    full_name  VARCHAR(24) NOT NULL,

    created_at BIGINT      NOT NULL DEFAULT 0
);

CREATE INDEX user_username_index ON user (username);

CREATE TABLE user_device
(
    user_id    INTEGER NOT NULL,
    device_id  INTEGER NOT NULL,
    created_at BIGINT  NOT NULL default 0
);

CREATE INDEX user_device__user_id__index ON user_device (user_id);
CREATE INDEX user_device__device_id__index ON user_device (device_id);
CREATE UNIQUE INDEX user_device__user_id__device_id__unique_index on user_device (user_id, device_id);

INSERT INTO user
VALUES (0, 'admin', '123456', 'admin', 0);
