package web

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
	"strings"
	"xming.club/tralwa/gas/context"
)

type Handler interface{}

type Router struct {
	R              chi.Router
	curGroupPrefix string
	curMiddlewares []Handler
}

func NewRouter() *Router {
	return &Router{chi.NewRouter(), "", []Handler{}}
}

func (r *Router) Use(middlewares ...Handler) {
	if r.curGroupPrefix != "" {
		r.curMiddlewares = append(r.curMiddlewares, middlewares...)
		return
	}
	for _, m := range middlewares {
		switch t := m.(type) {
		case func(http.Handler) http.Handler:
			r.R.Use(t)
		case func(*context.Context):
			r.R.Use(Middle(t))
		default:
			panic(fmt.Sprintf("Unsupported middleware type: %#v", t))
		}
	}
}

func Middle(f func(*context.Context)) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
			ctx := context.GetContext(req)
			f(ctx)
			if ctx.Written() {
				return
			}
			next.ServeHTTP(ctx.Resp, ctx.Req)
		})
	}
}

func (r *Router) Group(pattern string, fn func(), middlewares ...Handler) {
	preGroupPrefix := r.curGroupPrefix
	preMiddlewares := r.curMiddlewares
	r.curGroupPrefix += pattern
	r.curMiddlewares = append(r.curMiddlewares, middlewares...)

	fn()

	r.curGroupPrefix = preGroupPrefix
	r.curMiddlewares = preMiddlewares
}

func (r *Router) Mount(pattern string, subR *Router) {
	middlewares := make([]Handler, len(r.curMiddlewares))
	copy(middlewares, r.curMiddlewares)
	subR.Use(middlewares...)
	r.R.Mount(r.getPattern(pattern), subR.R)
}

func (r *Router) getMiddlewares(h []Handler) []Handler {
	var middlewares = make([]Handler, len(r.curMiddlewares), len(r.curMiddlewares)+len(h))
	copy(middlewares, r.curMiddlewares)
	middlewares = append(middlewares, h...)
	return middlewares
}

func (r *Router) getPattern(pattern string) string {
	newPattern := r.curGroupPrefix + pattern
	if !strings.HasPrefix(newPattern, "/") {
		newPattern = "/" + newPattern
	}
	if newPattern == "/" {
		return newPattern
	}
	return strings.TrimSuffix(newPattern, "/")
}

func (r *Router) Route(pattern string, methods string, handlers ...Handler) {
	p := r.getPattern(pattern)
	ms := strings.Split(methods, ",")
	var middlewares = r.getMiddlewares(handlers)
	for _, method := range ms {
		r.R.MethodFunc(strings.TrimSpace(method), p, Wrap(middlewares))
	}
}

func (r *Router) Any(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.HandleFunc(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Get(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Get(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Post(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Post(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Put(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Put(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Delete(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Delete(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Patch(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Patch(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Head(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Head(r.getPattern(pattern), Wrap(middlewares))
}

func (r *Router) Handle(pattern string, handlers ...Handler) {
	var middlewares = r.getMiddlewares(handlers)
	r.R.Handle(r.getPattern(pattern), Wrap(middlewares))
}

func Wrap(handlers []Handler) http.HandlerFunc {
	if len(handlers) == 0 {
		panic("No handlers found")
	}

	for _, handler := range handlers {
		switch t := handler.(type) {
		case http.HandlerFunc, func(http.ResponseWriter, *http.Request),
			func(*context.Context),
			func(http.Handler) http.Handler:
		default:
			panic(fmt.Sprintf("Unsupported handler type: %#v", t))
		}
	}

	return func(resp http.ResponseWriter, req *http.Request) {
		for i := 0; i < len(handlers); i++ {
			handler := handlers[i]
			switch t := handler.(type) {
			case http.HandlerFunc:
				t(resp, req)
				if r, ok := resp.(context.ResponseWriter); ok && r.Status() > 0 {
					return
				}
			case func(http.ResponseWriter, *http.Request):
				t(resp, req)
				if r, ok := resp.(context.ResponseWriter); ok && r.Status() > 0 {
					return
				}
			case func(*context.Context):
				ctx := context.GetContext(req)
				t(ctx)
				if ctx.Written() {
					return
				}
			case func(http.Handler) http.Handler:
				var next = http.HandlerFunc(func(http.ResponseWriter, *http.Request) {})
				if len(handlers) > i+1 {
					next = Wrap(handlers[i+1:])
				}
				t(next).ServeHTTP(resp, req)
				return
			default:
				panic(fmt.Sprintf("Unsupported handler type: %#v", t))
			}
		}
	}
}

func (r *Router) ServeHTTP(w http.ResponseWriter, q *http.Request) {
	r.R.ServeHTTP(w, q)
}

type Combo struct {
	r       *Router
	pattern string
	h       []Handler
}

func (r *Router) Combo(pattern string, h ...Handler) *Combo {
	return &Combo{r: r, pattern: pattern, h: h}
}

func (c *Combo) Get(h ...Handler) *Combo {
	c.r.Get(c.pattern, append(c.h, h...)...)
	return c
}

func (c *Combo) Post(h ...Handler) *Combo {
	c.r.Post(c.pattern, append(c.h, h...)...)
	return c
}

func (c *Combo) Put(h ...Handler) *Combo {
	c.r.Put(c.pattern, append(c.h, h...)...)
	return c
}

func (c *Combo) Delete(h ...Handler) *Combo {
	c.r.Delete(c.pattern, append(c.h, h...)...)
	return c
}

func (c *Combo) Patch(h ...Handler) *Combo {
	c.r.Patch(c.pattern, append(c.h, h...)...)
	return c
}

func (c *Combo) Head(h ...Handler) *Combo {
	c.r.Head(c.pattern, append(c.h, h...)...)
	return c
}
