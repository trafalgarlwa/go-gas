package web

import (
	"fmt"
	"gitea.com/go-chi/session"
	_ "gitea.com/go-chi/session/redis"
	"github.com/go-chi/chi/v5/middleware"
	"xming.club/tralwa/gas/api"
	"xming.club/tralwa/gas/api/admin"
	"xming.club/tralwa/gas/api/console"
	"xming.club/tralwa/gas/api/subscribe"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/setting"
)

func DefaultRouter() *Router {
	router := NewRouter()

	router.Use(session.Sessioner(session.Options{
		Provider: "redis",
		ProviderConfig: fmt.Sprintf("addr=%s:%s,prefix=session:",
			setting.Redis.Host, setting.Redis.Port),
		CookieName:  "i_like_xming",
		Gclifetime:  86400,
		Maxlifetime: 86400,
	}))

	router.Use(middleware.Logger)
	router.Use(context.Contexter)

	RegisterConsoleRoutes(router)
	RegisterApiRoutes(router)

	return router
}

func RegisterApiRoutes(r *Router) {
	r.Group("/api", func() {
		r.Group("", func() {
			r.Combo("/user").Get(api.UserInfo).Patch(api.UpdateUserInfo)
			r.Group("/devices", func() {
				r.Get("/bind", api.BindDeviceUUID)
			})
			r.Group("/device", func() {
				r.Combo("").Get(api.ListBoundDevices).Post(api.CreateDevice)
				r.Group("/{deviceID}", func() {
					r.Put("", api.UpdateDeviceName)
					r.Get("/record", api.ListRecordsFromRedis)
					r.Get("/records", api.ListRecordsByDeviceID)
					r.Combo("/bind").Post(api.BindDevice).Delete(api.UnBindDevice)
					r.Combo("/task").Post(api.DeviceStart).Delete(api.DeviceStop).Head(api.DeviceState).Put(api.DeviceExposed)
				}, api.RequireDeviceID)
			})
		}, api.RequireLogin)

		r.Post("/auth", api.LoginPost)
		r.Get("/subscribe/{userID}", subscribe.ServiceSubscribeWithRedis)
		r.Get("/qrcode", api.NewQRCode)
	})
}

func RegisterConsoleRoutes(r *Router) {
	r.Group("", func() {
		r.Any("", console.ListDevices)
		r.Group("/console", func() {
			r.Combo("/devices").Get(admin.ListDevices).Post(admin.BatchCreateDevice)
			r.Put("/device/{deviceID}/task", api.RequireDeviceID, api.DeviceExposed)
		})
	}, admin.RequireAdmin)
}
