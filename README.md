# go-gas

气体报警后台管理系统

__________________

## 安装环境
* Go 1.10 或者更高
* Mysql 5.x 或者更高

## 安装步骤

```shell
$ git clone https://git.xming.club/xming/go-gas
$ cd go-gas
```
确保 git 已经安装并且配置在环境变量中

## 运行

1. 设置 golang 包下载服务器为国内代理地址，如果不配置，部分包无法下载
```shell
export GOPROXY="https://goproxy.cn,direct"
```

2. 配置运行时选项
```shell
export db_username=root
export db_password=123456
export db_host=localhost
export db_port=3306
```
其中 db_host 和 db_port 是可选项，不填写则默认为 localhost:3306

3. 导入数据库文件

mysql 数据库文件为项目根目录下 mysql.sql

使用 mysql 登录数据库，然后执行以下命令建立数据库
```shell
mysql> source ./mysql.sql
```

4. 运行项目
```shell
$ go build -o app
$ ./app
```
