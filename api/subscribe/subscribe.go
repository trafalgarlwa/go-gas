package subscribe

import (
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/service/redis"
)

var upgrader = websocket.Upgrader{}

type Message struct {
	Type      string `json:"type"`
	Upper     string `json:"upper"`
	Lower     string `json:"lower"`
	Env       string `json:"env"`
	CreatedAt string `json:"created_at"`
}

func ServiceSubscribeWithRedis(ctx *context.Context) {
	userID, err := strconv.Atoi(ctx.URLParam("userID"))
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusBadRequest)
		return
	}
	record, err := redis.PopRecord(userID)
	if err != nil {
		ctx.Status(http.StatusNotFound)
		return
	}
	format := func(p model.Percentage) string { return fmt.Sprintf("%.2f%%", p*100) }
	message := Message{
		Type:      "high",
		Upper:     format(record.UpperLimit),
		Lower:     format(record.LowerLimit),
		Env:       format(record.Env),
		CreatedAt: record.CreatedAt.FormatDateTime(),
	}
	ctx.Json(http.StatusOK, message)
}
