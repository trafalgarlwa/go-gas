package api

import (
	"testing"
	"time"
	"xming.club/tralwa/gas/service/rabbit"
)

func Test_addAlkaneRecord(t *testing.T) {
	rabbit.ListenInsertRecord()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	done := make(chan bool)
	go func() {
		time.Sleep(time.Second * 10)
		done <- true
	}()

	for {
		select {
		case <-done:
			rabbit.Close()
			return
		case <-ticker.C:
			addAlkaneRecord()
		}
	}
}
