package api

import (
	"database/sql"
	"github.com/google/uuid"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/service/task"
)

const deviceIDKey = "deviceID"

func CreateDevice(ctx *context.Context) {
	deviceName := ctx.Req.FormValue("name")
	if len(strings.TrimSpace(deviceName)) == 0 {
		ctx.Json(http.StatusBadRequest, Response{"The field 'name' is blank"})
		return
	}
	device := &model.Device{
		UUID: uuid.NewString(),
		Name: deviceName,
	}
	err := model.AddDevice(device)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Json(http.StatusCreated, struct {
		Name string `json:"name"`
		UUID string `json:"uuid"`
	}{device.Name, device.UUID})
}

func RequireDeviceID(ctx *context.Context) {
	param := ctx.URLParam(deviceIDKey)
	id, err := strconv.Atoi(param)
	if err != nil {
		ctx.Json(http.StatusBadRequest, Response{"Format error with device id"})
		return
	}
	if ok := model.DeviceExist(id); !ok {
		ctx.Json(http.StatusNotFound, Response{"THe device not found"})
		return
	}
	ctx.Data[deviceIDKey] = id
}

func BindDevice(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	err := model.BindUserDevice(ctx.User.ID, id)
	if err != nil {
		if _, ok := err.(*model.ErrUserDeviceBound); ok {
			ctx.Json(http.StatusConflict, Response{"The device already bound"})
			return
		}
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusCreated)
}

func UpdateDeviceName(ctx *context.Context) {
	name := ctx.Req.FormValue("name")
	if len(strings.TrimSpace(name)) == 0 {
		ctx.Json(http.StatusBadRequest, Response{"The field 'name' is blank"})
		return
	}
	id := ctx.Data[deviceIDKey].(int)
	d := &model.Device{ID: id, Name: name}
	err := model.UpdateDevice(d)
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusAccepted)
}

func UnBindDevice(ctx *context.Context) {
	deviceID := ctx.Data[deviceIDKey].(int)
	err := model.RemoveUserDevice(ctx.User.ID, deviceID)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusNoContent)
}

func ListBoundDevices(ctx *context.Context) {
	devices, err := model.GetBoundDeviceByUID(ctx.User.ID)
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	type device struct {
		Running bool `json:"running"`
		*model.Device
	}
	deviceList := []*device{}
	for _, d := range devices {
		deviceList = append(deviceList, &device{task.DefaultController().IsRun(d.ID), d})
	}
	ctx.Json(http.StatusOK, deviceList)
}

func DeviceStart(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	task.DefaultController().Start(id)
	ctx.Status(http.StatusCreated)
}

func DeviceStop(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	task.DefaultController().Stop(id)
	ctx.Status(http.StatusNoContent)
}

func DeviceExposed(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	task.DefaultController().Expose(id)
	ctx.Status(http.StatusCreated)
}

func DeviceState(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	if ok := task.DefaultController().IsRun(id); ok {
		ctx.Status(http.StatusOK)
	} else {
		ctx.Status(http.StatusNotFound)
	}
}

func BindDeviceUUID(ctx *context.Context) {
	uuidParam := ctx.Req.URL.Query().Get("uuid")
	uuidParam = strings.TrimSpace(uuidParam)
	ok, _ := regexp.Match("[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}", []byte(uuidParam))
	if !ok {
		ctx.Json(http.StatusBadRequest, Response{"UUID format error"})
		return
	}
	err := model.BindDeviceWithUUID(ctx.User.ID, uuidParam)
	if err != nil {
		if err == sql.ErrNoRows {
			ctx.Status(http.StatusNotFound)
			return
		}
		log.Println(err)
	}
	ctx.Status(http.StatusCreated)
}
