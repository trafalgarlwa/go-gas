package admin

import (
	"fmt"
	"github.com/asaskevich/govalidator"
	"github.com/google/uuid"
	"log"
	"net/http"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
)

var keySecret = "heminghu00"

func init() {
	log.Printf("The admin secret is: %s", keySecret)
}

func RequireAdmin(ctx *context.Context) {
	secret := ctx.Req.URL.Query().Get("secret")
	if secret != keySecret {
		ctx.Status(http.StatusForbidden)
	}
}

func ListDevices(ctx *context.Context) {
	type deviceInfo struct {
		QRCodeLink string `json:"qrcode_link"`
		*model.DeviceWithUsers
	}
	deviceList := []*deviceInfo{}
	list, err := model.ListUserDevice()
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	for _, d := range list {
		deviceList = append(deviceList, &deviceInfo{fmt.Sprintf("http://hmh.xming.club:3000/api/devices/bind?uuid=%s", d.DeviceUUID), d})
	}
	ctx.Json(http.StatusOK, deviceList)
}

func BatchCreateDevice(ctx *context.Context) {
	countParam := ctx.Req.URL.Query().Get("count")
	deviceName := ctx.Req.URL.Query().Get("name")
	count, err := govalidator.ToInt(countParam)
	if err != nil {
		ctx.Status(http.StatusBadRequest)
		return
	}

	devices := []*model.Device{}
	for i := 0; i < int(count); i++ {
		devices = append(devices, &model.Device{UUID: uuid.NewString(), Name: deviceName})
	}
	err = model.BatchInsertDevice(devices)
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(http.StatusCreated)
}
