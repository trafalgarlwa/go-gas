package api

import (
	"github.com/skip2/go-qrcode"
	"net/http"
	"strings"
	"xming.club/tralwa/gas/context"
)

func NewQRCode(ctx *context.Context) {
	text := ctx.Req.URL.Query().Get("text")
	if text = strings.TrimSpace(text); len(text) == 0 {
		ctx.Status(http.StatusBadRequest)
		return
	}
	bytes, err := qrcode.Encode(text, qrcode.Medium, 256)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Resp.Header().Set("Content-Type", "image/png")
	ctx.Resp.Write(bytes)
}
