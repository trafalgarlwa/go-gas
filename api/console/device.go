package console

import (
	"fmt"
	"log"
	"net/http"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
)

func ListDevices(ctx *context.Context) {
	devices, err := model.ListUserDevice()
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	type item struct {
		Link string
		*model.DeviceWithUsers
	}
	var items []item
	for _, d := range devices {
		text := fmt.Sprintf("http://hmh.xming.club:3000/api/devices/bind?uuid=%s", d.DeviceUUID)
		link := "/api/qrcode?text=" + text
		items = append(items, item{link, d})
	}
	ctx.Data["Devices"] = items
	ctx.Html(200, "index")
}
