package api

import (
	"log"
	"net/http"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
)

type User struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	CreatedAt int64  `json:"created_at"`
}

func UserInfo(ctx *context.Context) {
	u := &User{
		ID:        ctx.ID,
		Name:      ctx.FullName,
		CreatedAt: int64(ctx.CreatedAt),
	}
	ctx.Json(http.StatusOK, u)
}

func UpdateUserInfo(ctx *context.Context) {
	name := ctx.Req.PostFormValue("name")
	if len(name) == 0 {
		ctx.Status(http.StatusNoContent)
		return
	}
	err := model.UpdateUserNickname(name, ctx.User.ID)
	if err != nil {
		log.Println(err)
		ctx.Status(http.StatusBadRequest)
		return
	}
	ctx.Status(http.StatusNoContent)
}
