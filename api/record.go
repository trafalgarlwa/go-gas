package api

import (
	"net/http"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/model/timestamp"
	"xming.club/tralwa/gas/service/rabbit"
	"xming.club/tralwa/gas/service/redis"
)

func ListRecordsFromRedis(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	duration := timestamp.Minute
	records := redis.ListRecordsSince(duration, id)
	ctx.Json(http.StatusOK, records)
}

// This function is used to test redis, it should be removed in future
// TODO remove this function and test file
func addAlkaneRecord() {
	mixture := model.NewMixture()
	record := &model.AlkaneRecord{
		ID:         0,
		DeviceID:   1,
		Ch4:        mixture[model.KindCH4].VolumeRatio,
		C2h4:       mixture[model.KindC2H4].VolumeRatio,
		C3h8:       mixture[model.KindC3H8].VolumeRatio,
		C4h8:       mixture[model.KindC4H8].VolumeRatio,
		LowerLimit: mixture.LowerLimit(),
		UpperLimit: mixture.UpperLimit(),
		CreatedAt:  timestamp.Now(),
	}
	go redis.AddRecord(record)
	go rabbit.SendRecord(record)
}

func ListRecordsByDeviceID(ctx *context.Context) {
	id := ctx.Data[deviceIDKey].(int)
	records, err := model.ListAllRecordsByDeviceID(id)
	if err != nil {
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Json(http.StatusOK, records)
}
