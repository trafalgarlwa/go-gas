package api

import (
	"net/http"
	"xming.club/tralwa/gas/context"
	"xming.club/tralwa/gas/model"
)

func LoginPost(ctx *context.Context) {
	if ctx.IsSigned() {
		ctx.Redirect("/")
		return
	}

	var username string
	var password string

	if username = ctx.Req.PostFormValue("username"); len(username) == 0 {
		ctx.Status(http.StatusBadRequest)
		return
	}
	if password = ctx.Req.PostFormValue("password"); len(password) == 0 {
		ctx.Status(http.StatusBadRequest)
		return
	}
	if user, ok := model.Authenticate(username, password); ok {
		ctx.Session.Set("uid", user.ID)
		ctx.Session.Set("uname", user.Username)
		ctx.Status(http.StatusOK)
	} else {
		ctx.Json(http.StatusBadRequest, Response{"Authentication failed"})
	}
}

func RequireLogin(ctx *context.Context) {
	if !ctx.IsSigned() {
		ctx.Json(http.StatusUnauthorized, Response{"Unauthorized"})
	}
}
