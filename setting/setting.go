package setting

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

var (
	IsWindows bool

	AppPath     string
	AppWorkPath string

	AppDataPath string

	Database = struct {
		Name     string
		Username string
		Password string
		Host     string
		Port     string
	}{
		Name: "gas_db",
		Host: "127.0.0.1",
		Port: "3306",
	}

	Redis = struct {
		Host string
		Port string
	}{
		Host: "127.0.0.1",
		Port: "6379",
	}

	Rabbit = struct {
		Host string
		Port string
	}{
		Host: "127.0.0.1",
		Port: "5672",
	}

	Python = struct {
		Host string
		Port string
	}{
		Host: "127.0.0.1",
		Port: "5000",
	}
)

func init() {
	IsWindows = runtime.GOOS == "windows"
	var err error
	if AppPath, err = getAppPath(); err != nil {
		log.Fatalf("Failed to get app path: %v", err)
	}
	AppWorkPath = getWorkPath()
	AppDataPath = path.Join(AppWorkPath, "data")

	InitConfig()
}

func getWorkPath() string {
	workPath := AppWorkPath
	if appWorkPath, ok := os.LookupEnv("APP_WORK_DIR"); ok {
		workPath = appWorkPath
	}
	if len(workPath) == 0 {
		i := strings.LastIndex(AppPath, "/")
		if i == -1 {
			workPath = AppPath
		} else {
			workPath = AppPath[:i]
		}
	}
	return strings.ReplaceAll(workPath, "\\", "/")
}

func getAppPath() (string, error) {
	var appPath string
	var err error
	if IsWindows && filepath.IsAbs(os.Args[0]) {
		appPath = filepath.Clean(os.Args[0])
	} else {
		appPath, err = exec.LookPath(os.Args[0])
	}
	if err != nil {
		return "", err
	}
	appPath, err = filepath.Abs(appPath)
	if err != nil {
		return "", err
	}
	return strings.ReplaceAll(appPath, "\\", "/"), err
}

func getEnv(key string, def ...string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	if len(def) > 0 {
		return def[0]
	}
	return ""
}

func InitConfig() {
	Database.Username = getEnv("DB_USER")
	Database.Password = getEnv("DB_PASSWORD")
	Database.Host = getEnv("DB_HOST", Database.Host)
	Database.Port = getEnv("DB_PORT", Database.Port)

	Redis.Host = getEnv("REDIS_HOST", Redis.Host)
	Redis.Port = getEnv("REDIS_PORT", Redis.Port)

	Rabbit.Host = getEnv("RABBIT_HOST", Rabbit.Host)
	Rabbit.Port = getEnv("RABBIT_PORT", Rabbit.Port)

	Python.Host = getEnv("PYTHON_HOST", Python.Host)
	Python.Port = getEnv("PYTHON_PORT", Python.Port)
}

func DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		Database.Username,
		Database.Password,
		Database.Host,
		Database.Port,
		Database.Name)
}
