package main

import (
	"log"
	"net/http"
	"xming.club/tralwa/gas/service/task"
	"xming.club/tralwa/gas/web"
)

func main() {
	task.Start()

	router := web.DefaultRouter()

	log.Println("Service start at port 3000")
	log.Fatal(http.ListenAndServe(":3000", router))
}
