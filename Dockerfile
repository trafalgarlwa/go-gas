FROM golang:latest as builder

WORKDIR /app
COPY . .
ENV GOPROXY="https://goproxy.cn,direct"
RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -ldflags="-s -w" -o app main.go

FROM scratch

COPY --from=builder /app/app .
COPY --from=builder /app/templates ./templates

ENTRYPOINT ["./app"]

EXPOSE 3000
