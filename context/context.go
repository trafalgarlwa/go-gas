package context

import (
	"context"
	"gitea.com/go-chi/session"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"log"
	"net/http"
	"xming.club/tralwa/gas/model"
	renderer "xming.club/tralwa/gas/render"
	sess "xming.club/tralwa/gas/session"
)

type Context struct {
	Resp    ResponseWriter
	Req     *http.Request
	Data    map[string]interface{}
	Session session.Store
	Render  *renderer.Render

	*model.User
}

var (
	contextKey interface{} = "default_context"
)

func WithContext(req *http.Request, ctx *Context) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), contextKey, ctx))
}

func WithValue(req *http.Request, key interface{}, val interface{}) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), key, val))
}

func GetContext(req *http.Request) *Context {
	return req.Context().Value(contextKey).(*Context)
}

func Contexter(next http.Handler) http.Handler {
	var rnd = renderer.New(renderer.Options{Extensions: []string{".html", ".tmpl"}})

	return http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		ctx := &Context{
			Resp:    NewResponse(writer),
			Session: session.GetSession(req),
			Data:    make(map[string]interface{}),
			Render:  rnd,
		}
		ctx.Req = WithContext(req, ctx)

		ctx.User = sess.User(ctx.Session)

		next.ServeHTTP(ctx.Resp, ctx.Req)
	})
}

func (ctx *Context) Written() bool {
	return ctx.Resp.Status() > 0
}

func (ctx *Context) URLParam(key string) string {
	return chi.URLParam(ctx.Req, key)
}

func (ctx *Context) Status(status int) {
	ctx.Resp.WriteHeader(status)
}

func (ctx *Context) Json(status int, v ...interface{}) {
	ctx.Resp.Header().Set("Content-Type", "application/json; charset=utf-8")
	ctx.Status(status)
	if len(v) > 0 {
		render.JSON(ctx.Resp, ctx.Req, v[0])
	}
}

func (ctx *Context) Html(status int, name string) {
	ctx.Resp.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := ctx.Render.Html(ctx.Resp, name, ctx.Data); err != nil {
		log.Println(err)
		ctx.Status(http.StatusInternalServerError)
		return
	}
	ctx.Status(status)
}

func (ctx *Context) Redirect(location string, status ...int) {
	code := http.StatusFound
	if len(status) > 0 {
		code = status[0]
	}
	http.Redirect(ctx.Resp, ctx.Req, location, code)
}

func (ctx *Context) IsSigned() bool {
	return ctx.User != nil
}
