package session

import (
	"gitea.com/go-chi/session"
	"xming.club/tralwa/gas/model"
)

func User(store session.Store) *model.User {
	uid, ok := store.Get("uid").(int)
	if !ok {
		return nil
	}
	user, err := model.GetUserByID(uid)
	if err != nil {
		return nil
	}
	return user
}
