package model

import (
	"encoding/json"
	"xming.club/tralwa/gas/model/timestamp"
)

type AlkaneRecord struct {
	ID         int                 `json:"id" db:"id"`
	DeviceID   int                 `json:"device_id" db:"device_id"`
	Env        Percentage          `json:"env" db:"env"`
	Ch4        Percentage          `json:"ch4" db:"ch4"`
	C2h4       Percentage          `json:"c2h4" db:"c2h4"`
	C3h8       Percentage          `json:"c3h8" db:"c3h8"`
	C4h8       Percentage          `json:"c4h8" db:"c4h8"`
	LowerLimit Percentage          `json:"lower_limit" db:"lower_limit"`
	UpperLimit Percentage          `json:"upper_limit" db:"upper_limit"`
	CreatedAt  timestamp.Timestamp `json:"created_at" db:"created_at"`
	Exposed    int                 `json:"exposed" db:"exposed"`
}

func (a AlkaneRecord) MarshalBinary() (data []byte, err error) {
	return json.Marshal(a)
}

func (a *AlkaneRecord) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, a)
}

func InsertAlkaneRecord(record *AlkaneRecord) error {
	s := `
INSERT INTO device_record
    (device_id,
     created_at, 
     env,
     ch4,
     c2h4, 
     c3h8,
     c4h8, 
     lower_limit,
     upper_limit,
     exposed)
VALUES (:device_id,
        :created_at,
        :env,
        :ch4,
        :c2h4,
        :c3h8,
        :c4h8,
        :lower_limit,
        :upper_limit,
        :exposed)`
	_, err := db.NamedExec(s, record)
	return err
}

func ListAllRecordsByDeviceID(deviceID int) ([]*AlkaneRecord, error) {
	records := make([]*AlkaneRecord, 0)
	sql := "SELECT id, device_id, env, ch4, c2h4, c3h8, c4h8, lower_limit, upper_limit, created_at FROM device_record WHERE device_id=?"
	err := db.Select(&records, sql, deviceID)
	return records, err
}
