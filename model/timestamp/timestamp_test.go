package timestamp

import (
	"encoding/json"
	"strconv"
	"testing"
)

func TestNow(t *testing.T) {
	ts := 1619101427
	timestamp := Timestamp(ts)
	if timestamp.MillSecond() != int64(ts*1000) {
		t.Errorf("Want %v bug got %v", ts*1000, timestamp.MillSecond())
	}
}

func TestTimestamp_MarshalJSON(t *testing.T) {
	ts := 1619101427000
	timestamp := Timestamp(ts)
	bytes, err := timestamp.MarshalJSON()
	if err != nil {
		t.Error(err)
	}
	t.Log(string(bytes))
	if strconv.FormatInt(int64(ts), 10) != string(bytes) {
		t.Error()
	}
}

func TestTimestamp_UnmarshalJSON(t *testing.T) {
	ts := 1619101427000
	timestamp := Timestamp(ts)
	bytes, _ := timestamp.MarshalJSON()

	var tm Timestamp
	err := json.Unmarshal(bytes, &tm)
	if err != nil {
		t.Error(err)
	}
	if int64(ts) != int64(tm) {
		t.Errorf("Want %v but got %v", ts, tm)
	}
}
