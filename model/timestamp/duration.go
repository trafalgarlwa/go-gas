package timestamp

type Duration int64

const (
	MillSecond Duration = 1
	Second              = 1000 * MillSecond
	Minute              = 60 * Second
	Hour                = 60 * Minute
)

func (d Duration) Multi(duration int64) Duration {
	return d * Duration(duration)
}
