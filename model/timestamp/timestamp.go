package timestamp

import (
	"encoding/json"
	"time"
)

type Timestamp int64

func Now() Timestamp {
	return Timestamp(time.Now().UnixNano() / int64(time.Millisecond))
}

func (t Timestamp) Sub(duration Duration) Timestamp {
	return Timestamp(t.MillSecond() - int64(duration))
}

func (t Timestamp) Add(duration Duration) Timestamp {
	return Timestamp(t.MillSecond() + int64(duration))
}

func (t Timestamp) MillSecond() int64 {
	return int64(t)
}

func (t Timestamp) Second() int64 {
	return int64(t / Timestamp(Second))
}

func (t Timestamp) AsTime() time.Time {
	loc, _ := time.LoadLocation("UTC")
	return time.Unix(t.Second(), 0).In(loc)
}

// FormatDateTime format as yyyy-MM-dd HH:mm:ss
func (t Timestamp) FormatDateTime() string {
	return t.AsTime().String()[:19]
}

func (t Timestamp) MarshalJSON() ([]byte, error) {
	return json.Marshal(int64(t))
}

func (t *Timestamp) UnmarshalJSON(bytes []byte) error {
	var tm int64
	err := json.Unmarshal(bytes, &tm)
	*t = Timestamp(tm)
	return err
}
