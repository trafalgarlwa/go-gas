package model

import (
	"encoding/json"
	"math/rand"
	"strconv"
)

type Kind int

type KindMapper map[Kind]string

const (
	KindCH4 Kind = iota
	KindC2H4
	KindC3H8
	KindC4H8
)

var nameMapper = KindMapper{
	KindCH4:  "CH4",
	KindC2H4: "C2H4",
	KindC3H8: "C3H8",
	KindC4H8: "C4H8",
}

// Name return the name of the model
func (g Kind) Name() string {
	return nameMapper[g]
}

type Percentage float64

func (p Percentage) MarshalJSON() ([]byte, error) {
	return json.Marshal(float64(p))
}

func (p *Percentage) UnmarshalJSON(bytes []byte) error {
	float, err := strconv.ParseFloat(string(bytes), 64)
	if err != nil {
		return err
	}
	*p = Percentage(float)
	return nil
}

func (p Percentage) Value() float64 {
	return float64(p)
}

type Alkane struct {
	Kind
	NOxygen     int
	VolumeRatio Percentage
}

type AlkaneMixture map[Kind]Alkane

func NewMixture() AlkaneMixture {
	mix := AlkaneMixture{}
	mix[KindCH4] = Alkane{KindCH4, 3, 0.9517}
	mix[KindC2H4] = Alkane{KindC2H4, 4, 0.022}
	mix[KindC3H8] = Alkane{KindC3H8, 6, 0.0137}
	mix[KindC4H8] = Alkane{KindC4H8, 8, 0.0126}

	return mix
}

func SimulateMixture() AlkaneMixture {
	sum := 1.0
	ch4 := rand.Float64()*0.06 + 0.9
	sum -= ch4
	c2h4 := rand.Float64() * sum
	sum -= c2h4
	c3h8 := rand.Float64() * sum
	c4h8 := sum
	mix := AlkaneMixture{}
	mix[KindCH4] = Alkane{KindCH4, 3, Percentage(ch4)}
	mix[KindC2H4] = Alkane{KindC2H4, 4, Percentage(c2h4)}
	mix[KindC3H8] = Alkane{KindC3H8, 6, Percentage(c3h8)}
	mix[KindC4H8] = Alkane{KindC4H8, 8, Percentage(c4h8)}
	return mix
}

// UpperLimit represent the upper explosion limit
func (mixture AlkaneMixture) UpperLimit() Percentage {
	var sum Percentage
	for _, alkane := range mixture {
		sum += Percentage(alkane.NOxygen/3.0) * alkane.VolumeRatio
	}
	return 1 / 4.76 / sum
}

// LowerLimit represent the lower explosion limit
func (mixture AlkaneMixture) LowerLimit() Percentage {
	var sum Percentage
	for _, alkane := range mixture {
		sum += Percentage(2*alkane.NOxygen) * alkane.VolumeRatio
	}
	return 1 / 4.76 / sum
}
