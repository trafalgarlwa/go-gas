package model

import (
	"database/sql"
	"fmt"
	"xming.club/tralwa/gas/model/timestamp"
)

type UserDevice struct {
	UserID    int                 `db:"user_id"`
	DeviceID  int                 `db:"device_id"`
	CreatedAt timestamp.Timestamp `db:"created_at"`
}

type ErrUserDeviceBound struct {
	userID   int
	deviceID int
}

func (e *ErrUserDeviceBound) Error() string {
	return fmt.Sprintf("The device id %d already bound with user %d", e.userID, e.deviceID)
}

func BindUserDevice(userID, deviceID int) error {
	if UserDeviceBound(userID, deviceID) {
		return &ErrUserDeviceBound{userID, deviceID}
	}
	_, err := db.Exec("INSERT INTO user_device(user_id, device_id, created_at) VALUES (?,?,?)", userID, deviceID, timestamp.Now())
	return err
}

func BindDeviceWithUUID(userID int, deviceUUID string) error {
	var deviceID int
	err := db.Get(&deviceID, "SELECT id FROM device WHERE uuid=?", deviceUUID)
	if err != nil {
		return err
	}
	return BindUserDevice(userID, deviceID)
}

func UserDeviceBound(userID, deviceID int) bool {
	rows, _ := db.Query("SELECT 1 FROM user_device WHERE user_id=? AND device_id=?", userID, deviceID)
	defer rows.Close()
	return rows.Next()
}

func GetBoundDeviceByUID(uid int) ([]*Device, error) {
	devices := make([]*Device, 0)
	err := db.Select(&devices, "SELECT device_id as id, uuid, name FROM device, user_device WHERE device.id=user_device.device_id AND user_id=?", uid)
	return devices, err
}

// RemoveUserDevice unbind user and device
func RemoveUserDevice(uid, deviceID int) error {
	_, err := db.Exec("DELETE FROM user_device WHERE user_id=? AND device_id=?", uid, deviceID)
	return err
}

type DeviceWithUsers struct {
	DeviceID   int64       `json:"device_id"`
	DeviceUUID string      `json:"device_uuid"`
	DeviceName string      `json:"device_name"`
	Users      []*userItem `json:"users"`
}

type userItem struct {
	Uid      int64  `json:"uid"`
	Username string `json:"username"`
	Name     string `json:"name"`
}

type deviceUserItem struct {
	DeviceID   int64          `db:"device_id"`
	DeviceUUID string         `db:"device_uuid"`
	DeviceName string         `db:"device_name"`
	UserID     sql.NullInt64  `db:"user_id"`
	Username   sql.NullString `db:"username"`
	Name       sql.NullString `db:"name"`
}

func ListUserDevice() ([]*DeviceWithUsers, error) {
	var s = `
select device.id   as device_id,
       device.uuid as device_uuid,
       device.name as device_name,
       user_id,
       username,
       full_name   as name
from device
         left join (user
    left join user_device ud on user.id = ud.user_id) on device.id = device_id
`
	rows := make([]*deviceUserItem, 0)
	err := db.Select(&rows, s)
	if err != nil {
		return nil, err
	}
	mp := make(map[int64]*DeviceWithUsers, 0)
	for _, row := range rows {
		if val, ok := mp[row.DeviceID]; ok {
			if row.UserID.Valid {
				val.Users = append(val.Users, &userItem{row.UserID.Int64, row.Username.String, row.Name.String})
			}
		} else {
			var users []*userItem
			if row.UserID.Valid {
				users = []*userItem{{row.UserID.Int64, row.Username.String, row.Name.String}}
			} else {
				users = []*userItem{}
			}
			mp[row.DeviceID] = &DeviceWithUsers{row.DeviceID, row.DeviceUUID, row.DeviceName, users}
		}
	}
	res := make([]*DeviceWithUsers, 0)
	for _, elem := range mp {
		res = append(res, elem)
	}
	return res, nil
}

func ListUserIDByDeviceID(deviceID int) ([]int, error) {
	var ids []int
	err := db.Select(&ids, "SELECT user_id AS id FROM user_device WHERE device_id=?", deviceID)
	return ids, err
}
