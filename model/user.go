package model

import (
	"log"
	"xming.club/tralwa/gas/model/timestamp"
)

type User struct {
	ID       int    `db:"id"`
	Username string `db:"username"`
	Password string `db:"password"`
	FullName string `db:"full_name"`

	CreatedAt timestamp.Timestamp `db:"created_at"`
}

func GetUserByID(id int) (*User, error) {
	u := &User{}
	err := db.Get(u, "SELECT id, username, password, full_name, created_at FROM user WHERE id=?", id)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func GetUserByUsername(username string) (*User, error) {
	u := &User{}
	err := db.Get(u, "SELECT id, username, password, full_name, created_at FROM user WHERE username=?", username)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func UpdateUserNickname(nickname string, id int) error {
	_, err := db.Exec("UPDATE user SET full_name=? WHERE id=?", nickname, id)
	return err
}

func Authenticate(username, password string) (*User, bool) {
	user, err := GetUserByUsername(username)
	if err != nil {
		log.Println(err)
		return nil, false
	}

	if user.Password != password {
		return nil, false
	}
	return user, true
}
