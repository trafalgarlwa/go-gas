package model

import (
	"fmt"
)

type Device struct {
	ID   int    `json:"id" db:"id"`
	UUID string `json:"uuid" db:"uuid"`
	Name string `json:"name" db:"name"`
}

type ErrDeviceNotExist struct {
	deviceID int
}

func (e *ErrDeviceNotExist) Error() string {
	return fmt.Sprintf("The device not exist with id %d", e.deviceID)
}

func AddDevice(device *Device) error {
	_, err := db.Exec("INSERT INTO device(uuid, name) VALUES (?,?)", device.UUID, device.Name)
	return err
}

func BatchInsertDevice(devices []*Device) error {
	_, err := db.NamedExec("INSERT INTO device(uuid, name) VALUES (:uuid, :name)", devices)
	return err
}

func GetDeviceByUUID(uuid string) (*Device, error) {
	d := &Device{}
	err := db.Get(d, "SELECT id, uuid, name FROM device WHERE uuid=?", uuid)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func GetDeviceByID(id int) (*Device, error) {
	d := &Device{}
	err := db.Get(d, "SELECT id, uuid, name FROM device WHERE id=?", id)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func DeviceExist(deviceID int) bool {
	rows, _ := db.Queryx("SELECT 1 FROM device WHERE id = ?", deviceID)
	defer rows.Close()
	return rows.Next()
}

func UpdateDevice(device *Device) error {
	_, err := db.Exec("UPDATE device SET name=? WHERE id=?", device.Name, device.ID)
	return err
}
