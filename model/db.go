package model

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"xming.club/tralwa/gas/setting"
)

var db *sqlx.DB

func init() {
	var err error

	db, err = sqlx.Connect("mysql", setting.DSN())
	if err != nil {
		log.Fatal(err)
	}
}
