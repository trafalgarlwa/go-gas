module xming.club/tralwa/gas

go 1.16

require (
	gitea.com/go-chi/session v0.0.0-20210108030337-0cb48c5ba8ee
	github.com/RedisTimeSeries/redistimeseries-go v1.4.4
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/go-chi/chi/v5 v5.0.2
	github.com/go-chi/render v1.0.1
	github.com/go-redis/redis/v8 v8.8.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/jmoiron/sqlx v1.3.3
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/streadway/amqp v1.0.0
)
