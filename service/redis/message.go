package redis

import (
	"context"
	"fmt"
	rdb "github.com/go-redis/redis/v8"
	"log"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/setting"
)

var client = NewClient()
var background = context.TODO()

func NewClient() *rdb.Client {
	c := rdb.NewClient(&rdb.Options{
		Addr: fmt.Sprintf("%s:%s", setting.Redis.Host, setting.Redis.Port),
	})
	_, err := c.Ping(background).Result()
	if err != nil {
		log.Println(err)
		return nil
	}
	return c
}

func PushRecord(record *model.AlkaneRecord) {
	ids, err := model.ListUserIDByDeviceID(record.DeviceID)
	if err != nil {
		log.Println(err)
		return
	}
	for _, id := range ids {
		if err := client.LPush(background, fmt.Sprintf("user:record:%d", id), record).Err(); err != nil {
			log.Println(err)
		}
	}
}

func PopRecord(userID int) (*model.AlkaneRecord, error) {
	data, err := client.RPop(background, fmt.Sprintf("user:record:%d", userID)).Result()
	if err != nil {
		return nil, err
	}
	record := &model.AlkaneRecord{}
	record.UnmarshalBinary([]byte(data))
	return record, nil
}
