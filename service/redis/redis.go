package redis

import (
	"fmt"
	redistimeseries "github.com/RedisTimeSeries/redistimeseries-go"
	"log"
	"time"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/model/timestamp"
	"xming.club/tralwa/gas/setting"
)

type Redis struct {
	*redistimeseries.Client
}

var ctx = New()

const (
	FieldEnv        = "env"
	FieldCH4        = "ch4"
	FieldC2H4       = "c2h4"
	FieldC3H8       = "c3h8"
	FieldC4H8       = "c4h8"
	FieldLowerLimit = "lower_limit"
	FieldUpperLimit = "upper_limit"

	recordPrefix = "record:"
)

func New() *Redis {
	client := redistimeseries.NewClient(fmt.Sprintf("%s:%s", setting.Redis.Host, setting.Redis.Port), "sys", nil)
	return &Redis{client}
}

func AddRecord(record *model.AlkaneRecord) {
	type field struct {
		field string
		value float64
	}
	fields := []field{
		{FieldEnv, record.Env.Value()},
		{FieldCH4, record.Ch4.Value()},
		{FieldC2H4, record.C2h4.Value()},
		{FieldC3H8, record.C3h8.Value()},
		{FieldC4H8, record.C4h8.Value()},
		{FieldLowerLimit, record.LowerLimit.Value()},
		{FieldUpperLimit, record.UpperLimit.Value()}}
	for _, f := range fields {
		ctx.AddWithOptions(fmt.Sprintf("%s%d:%s", recordPrefix, record.DeviceID, f.field),
			record.CreatedAt.MillSecond(),
			f.value,
			redistimeseries.CreateOptions{
				RetentionMSecs: 24 * time.Hour,
			})
	}
}

func ListRecordsSince(duration timestamp.Duration, deviceID int) map[string]interface{} {
	records := make(map[string]interface{})
	now := timestamp.Now()
	from := now.Sub(duration)
	fields := []string{FieldCH4, FieldC2H4, FieldC3H8, FieldC4H8, FieldLowerLimit, FieldUpperLimit, FieldEnv}
	for _, f := range fields {
		key := fmt.Sprintf("%s%d:%s", recordPrefix, deviceID, f)
		points, err := ctx.RangeWithOptions(key, from.MillSecond(), now.MillSecond(), redistimeseries.DefaultRangeOptions)
		if err != nil {
			log.Print(err)
			continue
		}
		data := make([]*Point, 0)
		for _, point := range points {
			data = append(data, &Point{point.Timestamp, point.Value})
		}
		records[f] = data
	}
	return records
}
