package rabbit

import (
	"testing"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/model/timestamp"
)

func TestRabbit_AddRecord(t *testing.T) {
	record := &model.AlkaneRecord{
		ID:         0,
		DeviceID:   1,
		Ch4:        0.9517,
		C2h4:       0.022,
		C3h8:       0.0137,
		C4h8:       0.0126,
		LowerLimit: 0.05,
		UpperLimit: 0.21,
		CreatedAt:  timestamp.Now(),
	}
	SendRecord(record)
}
