package rabbit

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/setting"
)

type Rabbit struct {
	*amqp.Connection
	*amqp.Channel
	amqp.Queue
}

var ctx = New()

const DeviceRecordInsert = "device.record.insert"

func New() *Rabbit {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://guest:guest@%s:%s", setting.Rabbit.Host, setting.Rabbit.Port))
	if err != nil {
		log.Fatal(err)
	}
	ch, err := conn.Channel()
	if err != nil {
		log.Fatal(err)
	}
	queue, err := ch.QueueDeclare(DeviceRecordInsert, true, false, false, false, nil)
	if err != nil {
		log.Fatal(err)
	}
	return &Rabbit{conn, ch, queue}
}

func SendRecord(record *model.AlkaneRecord) {
	bytes, _ := json.Marshal(record)
	ctx.Publish("",
		DeviceRecordInsert,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "application/json",
			Body:         bytes,
		})
}

func ListenInsertRecord() {
	messages, err := ctx.Consume(DeviceRecordInsert, "", false, false, false, false, nil)
	if err != nil {
		log.Println(err)
		return
	}
	go func() {
		for msg := range messages {
			if msg.ContentType != "application/json" {
				continue
			}
			record := &model.AlkaneRecord{}
			err := json.Unmarshal(msg.Body, record)
			if err != nil {
				log.Println(err, "Failed to unmarshal record from message body")
				return
			}
			err = model.InsertAlkaneRecord(record)
			if err != nil {
				log.Println(err, "Failed to insert alkane record")
				return
			}
			msg.Ack(false)
		}
	}()
}

func ListExposedRecord(userID int) (<-chan amqp.Delivery, error) {
	return ctx.Consume(fmt.Sprintf("user.exposed.%d", userID), "consumer", false, false, false, false, nil)
}

func Close() (err error) {
	defer func(Conn *amqp.Connection) {
		err = Conn.Close()
		if err != nil {
			log.Println(err)
			return
		}
	}(ctx.Connection)
	defer func(ch *amqp.Channel) {
		err = ch.Close()
		if err != nil {
			log.Println(err)
			return
		}
	}(ctx.Channel)
	return nil
}
