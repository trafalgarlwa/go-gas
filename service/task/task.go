package task

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"sync"
	"time"
	"xming.club/tralwa/gas/model"
	"xming.club/tralwa/gas/model/timestamp"
	"xming.club/tralwa/gas/service/rabbit"
	"xming.club/tralwa/gas/service/redis"
	"xming.club/tralwa/gas/setting"
)

var c = &Controller{pool: make(map[int]*Task)}

type Controller struct {
	m    sync.Mutex
	pool map[int]*Task
}

type Task struct {
	deviceID int
	ticker   *time.Ticker
	exposed  chan bool
	done     chan bool
}

func (c *Controller) Start(deviceID int) {
	c.m.Lock()
	defer c.m.Unlock()
	if _, ok := c.pool[deviceID]; !ok {
		task := &Task{deviceID: deviceID, ticker: time.NewTicker(5 * time.Second), done: make(chan bool), exposed: make(chan bool, 10)}
		c.pool[deviceID] = task
		go task.PeriodUploadRecord()
	}
}

func (c *Controller) Stop(deviceID int) {
	c.m.Lock()
	defer c.m.Unlock()
	if task, ok := c.pool[deviceID]; ok {
		task.done <- true
		close(task.done)
		close(task.exposed)
		delete(c.pool, deviceID)
	}
}

func (c *Controller) Expose(deviceID int) {
	c.m.Lock()
	c.m.Unlock()
	if c.IsRun(deviceID) {
		c.pool[deviceID].exposed <- true
	}
}

func (c *Controller) IsRun(deviceID int) bool {
	_, ok := c.pool[deviceID]
	return ok
}

func Start() {
	go ListenInsertRecord()

	log.Println("Tasks start")
}

func DefaultController() *Controller {
	return c
}

func (t *Task) PeriodUploadRecord() {
	defer t.ticker.Stop()
	for {
		select {
		case <-t.ticker.C:
			mixture := model.SimulateMixture()
			var env model.Percentage
			var exposed = 0
			select {
			case <-t.exposed:
				difference := float64(mixture.UpperLimit() - mixture.LowerLimit())
				value := rand.Float64()*difference + float64(mixture.LowerLimit()) - 0.02
				env = model.Percentage(value)
				exposed = 1
			default:
				env = model.Percentage(rand.Float64() * (0.005))
			}

			record := &model.AlkaneRecord{
				ID:         0,
				DeviceID:   t.deviceID,
				Env:        env,
				Ch4:        mixture[model.KindCH4].VolumeRatio,
				C2h4:       mixture[model.KindC2H4].VolumeRatio,
				C3h8:       mixture[model.KindC3H8].VolumeRatio,
				C4h8:       mixture[model.KindC4H8].VolumeRatio,
				Exposed:    exposed,
				LowerLimit: mixture.LowerLimit(),
				UpperLimit: mixture.UpperLimit(),
				CreatedAt:  timestamp.Now(),
			}
			redis.AddRecord(record)
			rabbit.SendRecord(record)
			if exposed == 1 {
				go redis.PushRecord(record)
			}
		case <-t.done:
			return
		}
	}
}

func callExposed(lower, env model.Percentage) bool {
	values := url.Values{}
	values.Set("lower", fmt.Sprintf("%f", lower))
	values.Set("env", fmt.Sprintf("%f", env))
	resp, err := http.DefaultClient.PostForm(fmt.Sprintf("http://%s:%s", setting.Python.Host, setting.Python.Port), values)
	if err != nil {
		log.Println(err)
		return false
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return false
	}
	return string(bytes) == "True"
}

func ListenInsertRecord() {
	rabbit.ListenInsertRecord()
}
